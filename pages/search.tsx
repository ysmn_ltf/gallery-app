import type { NextPage } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

const User: NextPage = () => {
  const router = useRouter();
  const { keyword } = router.query;
  return (
    <div className="container">
      <Head>
        <title>Results for {keyword}</title>
      </Head>

      <h1 className="title">Results for {keyword}</h1>
    </div>
  );
};

export default User;
