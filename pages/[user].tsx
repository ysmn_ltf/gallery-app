import type { NextPage } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';

const User: NextPage = () => {
  const router = useRouter();
  const { user } = router.query;
  return (
    <div className="container">
      <Head>
        <title>{user}</title>
      </Head>

      <h1 className="title">{user}</h1>
    </div>
  );
};

export default User;
