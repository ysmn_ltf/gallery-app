import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
// import styles from '../styles/Home.module.scss';

const Home: NextPage = () => {
  return (
    <div className="container">
      <Head>
        <title>Gallery</title>
      </Head>

      <h1 className="title">Gallery</h1>
    </div>
  );
};

export default Home;
